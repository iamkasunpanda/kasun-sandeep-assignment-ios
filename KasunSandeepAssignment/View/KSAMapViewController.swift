//
//  KSAMapViewController.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/3/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import MapKit

class KSAMapViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    
    var hotelData :HotelModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configMap()
    }

    func configMap() {
        guard let lat = hotelData?.latitude, let long = hotelData?.longitude else {
            return
        }
        
        // MARK: Navigate to location
        let center = CLLocationCoordinate2D(latitude: Double(lat) ?? 0.0, longitude: Double(long) ?? 0.0)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.setRegion(region, animated: true)
        
        // MARK: Adding pointer
        let annotation = MKPointAnnotation()
        annotation.title = hotelData?.title
        annotation.subtitle = hotelData?.address
        annotation.coordinate = center
        mapView.addAnnotation(annotation)
    }
    
    @IBAction func actionBack(_ sender: UIBarButtonItem) {
        popMe()
    }
}
