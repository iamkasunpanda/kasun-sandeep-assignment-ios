//
//  KSAHotelListTableViewControllerManager.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/3/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import SDWebImage
import FBSDKCoreKit
import FBSDKLoginKit

extension KSAHotelListTableViewController :KSAHotelListHeaderTableViewCellDelegate {
    func didLogout() {
        AppDefault.shared.logout()
        navigateOverLogout()
    }
}

class KSAHotelListHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var fbLogoutBtnContainer: UIView!
    
    var delegate :KSAHotelListHeaderTableViewCellDelegate?
    
    override func awakeFromNib() {
        lblName.text = KSAFBManager.shared.user?.name
        lblEmail.text = KSAFBManager.shared.user?.email
        
        fbButtonConfig()
    }
    
    // MARK:- Config facebook button
    func fbButtonConfig() {
        guard let btnLogin = KSAFBManager.shared.button else {
            return
        }
        
        btnLogin.delegate = self
        
        fbLogoutBtnContainer.addSubview(btnLogin)
    }
}

extension KSAHotelListHeaderTableViewCell :LoginButtonDelegate {
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {}
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        delegate?.didLogout()
    }
}

class KSAHotelListTableViewCell: UITableViewCell {
    @IBOutlet weak var imgAvatar: UIImageView?
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {}
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {}
    
    func updateCell(_ data: HotelModel) {
        if let link = data.image?.small?.httpsURL() {
            imgAvatar?.sd_setImage(with: link, placeholderImage: UIImage(named: "default-image"))
        }
        
        lblTitle.text = data.title
        lblDescription.text = data.address
    }
}
