//
//  KSAHotelDetailTableViewController.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/3/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class KSAHotelDetailTableViewController: UITableViewController {
    let reuseIdentifier = "KSAHotelDetailTableViewCell"
    var hotelData :HotelModel?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        if let c = cell as? KSAHotelDetailTableViewCell, let data = hotelData {
            c.updateCell(data)
        }

        return cell
    }
    
    @IBAction func actionBack(_ sender: UIBarButtonItem) {
        popMe()
    }
    
    // MARK: Navigate to map view screen
    @IBAction func actionMap(_ sender: UIBarButtonItem) {
        if let vc = viewControllerMain(.mapView) as? KSAMapViewController {
            vc.hotelData = hotelData
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

class KSAHotelDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {}
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {}
    
    func updateCell(_ data: HotelModel) {
        if let link = data.image?.medium?.httpsURL() {
            imgAvatar?.sd_setImage(with: link, placeholderImage: UIImage(named: "default-image"))
        }
        
        lblTitle.text = data.title
        lblDescription.text = data.description
    }
}
