//
//  KSAHotelListTableViewController.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/2/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

protocol KSAHotelListHeaderTableViewCellDelegate {
    func didLogout()
}

class KSAHotelListTableViewController: UITableViewController {
    let reuseIdentifier = (header: "KSAHotelListHeaderTableViewCell", cell: "KSAHotelListTableViewCell")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return KSAHotelAPIManager.shared.hotelList.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier.header)
        
        if let c = cell as? KSAHotelListHeaderTableViewCell {
            c.delegate = self
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier.cell, for: indexPath)

        if let c = cell as? KSAHotelListTableViewCell {
            c.updateCell(KSAHotelAPIManager.shared.hotelList[indexPath.row])
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // MARK: Navigate to detail view screen
        if let vc = viewControllerMain(.hotelDetailView) as? KSAHotelDetailTableViewController {
            vc.hotelData = KSAHotelAPIManager.shared.hotelList[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

