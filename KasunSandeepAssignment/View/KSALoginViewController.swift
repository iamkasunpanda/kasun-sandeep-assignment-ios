//
//  KSALoginViewController.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/2/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class KSALoginViewController: UIViewController {
    @IBOutlet weak var fbLoginBtnContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fbButtonConfig()
    }
    
    // MARK:- Config facebook button
    func fbButtonConfig() {
        guard let btnLogin = KSAFBManager.shared.button else {
            return
        }
        
        btnLogin.delegate = self
        
        fbLoginBtnContainer.addSubview(btnLogin)
    }
}

extension KSALoginViewController :LoginButtonDelegate {
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        guard error == nil, result?.isCancelled == false else {
            return
        }
        
        // MARK:- Syncing initial data
        AppDefault.shared.initialUpdate { (status) in
            self.navigateOverLogin()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print(loginButton)
    }
}

