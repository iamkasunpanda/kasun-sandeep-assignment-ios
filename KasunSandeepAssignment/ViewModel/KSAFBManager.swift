//
//  KSAFBManager.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/2/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

struct UserStruct :Codable {
    var id: String
    var name: String
    var email: String
    
}

class KSAFBManager: NSObject {
    var button :FBLoginButton?
    var user :UserStruct?
    
    static var shared :KSAFBManager = {
        let model = KSAFBManager()
        
        model.button = FBLoginButton(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        model.button?.permissions = ["public_profile", "email"]
        
        return model
    }()
    
    func syncUserData(successCallback:@escaping (_ status :Bool)->Void) {
        let request = GraphRequest(graphPath: "me", parameters: ["fields":"email,name"], tokenString: AccessToken.current?.tokenString, version: nil, httpMethod: .get)
        request.start { (connection, data, error) in
            successCallback(error == nil)
            
            guard let result = data else {
                return
            }
            
            do {
                guard let jsonData = try? JSONSerialization.data(withJSONObject:result) else {
                    return
                }
                
                KSAFBManager.shared.user = try JSONDecoder().decode(UserStruct.self, from: jsonData)
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
    }

    var resetData :Void {
        get {
            let model = KSAFBManager.shared
            
            model.user = nil
        }
    }
}
