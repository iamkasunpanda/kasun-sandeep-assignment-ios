//
//  KSAHotelAPIManager.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/2/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class KSAHotelAPIManager: NSObject {
    var hotelList :[HotelModel] = []
    
    static var shared :KSAHotelAPIManager = {
        let model = KSAHotelAPIManager()
        return model
    }()
    
    func getHotelList(successCallback:@escaping (_ status :Bool)->Void) {
        HotelAPI.syncHotelList { (data, error) in
            print(data)
            if let list = data?.data {
                KSAHotelAPIManager.shared.hotelList = list
                successCallback(true)
            } else {
                successCallback(false)
            }
        }
    }
    
    var resetData :Void {
        get {
            let model = KSAHotelAPIManager.shared
            
            model.hotelList.removeAll()
        }
    }
}
