//
//  Helper.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/2/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

extension Array where Element: Equatable {
    var orderedSetEquatable: Array  {
        var array: [Element] = []
        return compactMap {
            if array.contains($0) {
                return nil
            } else {
                array.append($0)
                return $0
            }
        }
    }
}

extension String {
    
    func httpURL() -> URL {
        var urlstring = self
        if !urlstring.contains("http"), !urlstring.contains("https") {
            urlstring = "http:" + urlstring
        }
        
        if let url = URL(string: urlstring.replacingOccurrences(of: "https://", with: "http://")) {
            return url
        }
        
        return URL(string: "http://")!
    }
    
    func httpsURL() -> URL {
        var urlstring = self
        if !urlstring.contains("http"), !urlstring.contains("https") {
            urlstring = "https:" + urlstring
        }
        
        if let url = URL(string: urlstring.replacingOccurrences(of: "http://", with: "https://")) {
            return url
        }
        
        return URL(string: "https://")!
    }
    
}

extension UIViewController {
    func updateForLeftBarButtonAction() {
        let button = self.navigationItem.leftBarButtonItem
        button?.action = #selector(self.popMe)
    }
    
    func enableBackButton(enable : Bool) {
        let button = self.navigationItem.leftBarButtonItem
        button?.isEnabled = enable
        if enable == false {
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.clear
        } else {
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        }
    }
    
    
    @objc func popMe() {
        if self.navigationController == nil {
            if let index = self.tabBarController?.selectedIndex, let nvc = self.tabBarController?.children[index] as? UINavigationController {
                nvc.popViewController(animated: true)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
}
