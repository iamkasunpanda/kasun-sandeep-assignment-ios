//
//  SegueHelper.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/2/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

enum View: String {
    case loginView = "LoginViewController"
    case hotelListView = "HotelListTableViewController"
    case hotelDetailView = "KSAHotelDetailTableViewController"
    case mapView = "KSAMapViewController"
}

enum Storyboard: String {
    case main = "Main"
}

extension UIViewController {
    
    func storyboard(_ storyboard :Storyboard) -> UIStoryboard {
        return UIStoryboard(name: storyboard.rawValue, bundle: nil)
    }
    
    func viewControllerMain(_ view :View) -> UIViewController {
        return storyboard(.main).instantiateViewController(withIdentifier: view.rawValue)
    }
    
    func presentOverMain(_ view :View) {
        self.present(storyboard(.main).instantiateViewController(withIdentifier: view.rawValue), animated: true, completion: nil)
    }
    
    func navigationOverHereMain(_ view :View) {
        let vc = viewControllerMain(view)
        var vcs = self.navigationController?.children
        if let vcindex = vcs?.firstIndex(of: vc) {
            vcs?.remove(at: vcindex)
            if let svc = vcs?.orderedSetEquatable {
                self.navigationController?.setViewControllers(svc, animated: true)
            }
        } else {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func navigateOverLogin() {
        let vc = storyboard(.main).instantiateInitialViewController()!
        AppDelegate.shared.window?.rootViewController = vc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func navigateOverLogout() {
        let vc = viewControllerMain(View.loginView)
        AppDelegate.shared.window?.rootViewController = vc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
}
