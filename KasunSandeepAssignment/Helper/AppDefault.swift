//
//  AppDefault.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/3/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class AppDefault: NSObject {
    static var shared :AppDefault = {
        let model = AppDefault()
        return model
    }()
    
    
    func initialUpdate(callback :@escaping (_ success :Bool)->Void) {
        let dispatchGroupLoad = DispatchGroup()
        
        // MARK:- Sync user data
        dispatchGroupLoad.enter()
        KSAFBManager.shared.syncUserData { (status) in
            dispatchGroupLoad.leave()
        }
        
        // MARK:- Sync hotel list
        dispatchGroupLoad.enter()
        KSAHotelAPIManager.shared.getHotelList { (status) in
            dispatchGroupLoad.leave()
        }
        
        dispatchGroupLoad.notify(queue: .main) {
            callback(true)
        }
    }
    
    func logout() {
        /// Clear static data
        AppDefault.shared = AppDefault()
        KSAFBManager.shared.resetData
        KSAHotelAPIManager.shared.resetData
    }
}
