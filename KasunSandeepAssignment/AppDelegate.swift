//
//  AppDelegate.swift
//  KasunSandeepAssignment
//
//  Created by Kasun Sandeep on 7/2/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FBSDKCoreKit.ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        launchApplication()
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        return FBSDKCoreKit.ApplicationDelegate.shared.application(application, open: url, options: options)
    }

    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {}

    func launchApplication() {
        let storyboard = UIViewController().storyboard(.main)
        var vc: UIViewController = storyboard.instantiateInitialViewController()!
        
        // MARK:- Check Login status and navigate to hotel list view
        if AccessToken.isCurrentAccessTokenActive {
            // MARK:- Syncing initial data
            AppDefault.shared.initialUpdate { (status) in
                AppDelegate.shared.window?.rootViewController = vc
                AppDelegate.shared.window?.makeKeyAndVisible()
            }
        } else {
            vc = storyboard.instantiateViewController(withIdentifier: View.loginView.rawValue)
            AppDelegate.shared.window?.rootViewController = vc
            AppDelegate.shared.window?.makeKeyAndVisible()
        }
        
        
    }
}

